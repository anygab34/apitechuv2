package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    // Agregar Usuarios
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("El ID  que se va a crear es: " + user.getId());
        System.out.println("El nombre del usuario que se va a crear es: " + user.getName());
        System.out.println("La edad del usuario que se va a crear es: " + user.getAge());


        return new ResponseEntity<>(
                this.userService.addUser(user)
                , HttpStatus.OK
        );
    }

    //Obtener Usuarios con base a la edad
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam(name = "orderby", required = false) String orderBy
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de $orderby es " +orderBy);

        return new ResponseEntity<>(
                this.userService.getUsers(orderBy)
                , HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id, UserModel user) {
        System.out.println("getUserById");
        System.out.println("El ID del Usuario a buscar es: " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //Actualizar usuario

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("El Id del usuario a actualizar en parametro URL es" + id);
        System.out.println("El Id del usuario a actualizar es:" + user.getId());
        System.out.println("El nombre del usuario a actualizar es:" + user.getName());
        System.out.println("La edad del usuario a actualizar es:" + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()) {
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.userService.update(user);
        }
        return new ResponseEntity<>(
                user
                , userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //Borrar usuario por ID
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");

        boolean deletedProduct = this.userService.delete(id);
        return new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no borrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
